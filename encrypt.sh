#!/bin/bash
for file in `ls plain_secrets/*`
do
  for user_key in `ls public_keys/*`
  do
    username=`echo $user_key | awk '{print $2}' FS=[-]`
    filename=`echo $file | awk '{print $2}' FS=[/]`
    out=encrypted_secrets/${username}_${filename}_enc

    echo Encrypting $filename for $username in $out

    openssl smime -encrypt -aes256 -binary -outform DEM \
      -in $file \
      -out $out \
      $user_key
  done
done
