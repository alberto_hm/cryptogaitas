#!/bin/bash
for file in `ls encrypted_secrets/*`
do
  for user_key in `ls public_keys/*`
  do
    username=`echo $user_key | awk '{print $2}' FS=[-]`
    filename=`echo $file | awk '{print $2}' FS=[/]`
    outname=plain_secrets/${username}_${filename}
    outname=${outname%_enc}

    echo Decrypting $filename for $username to $outname
    openssl smime -decrypt -binary -inform DEM \
      -inkey ~/.ssh/cryptogaitas-$USER-private.pem \
      -in $file \
      -out $outname
  done
done
