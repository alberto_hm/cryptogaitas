# Security

In order to share sensitive data between the team members, we can use a RSA key.

In the first step, each member has to create a private/public keys. In your shell, type:

    openssl req -x509 -nodes -days 100000 -newkey rsa:2048 \
      -keyout cryptogaitas-$USER-private.pem \
      -out cryptogaitas-$USER-public.pem \
      -subj /

This command will create `cryptogaitas-$USER-private.pem` and `cryptogaitas-$USER-public.pem`. The `cryptogaitas-$USER-private.pem` **has to be protected**, and never be shared with anyone else. You should move it to your `~./ssh` directory:

    mv cryptogaitas-$USER-private.pem ~/.ssh/
    chmod 600 ~/.ssh/cryptogaitas-$USER-private.pem

Then, the public key should be added to the `public_keys` directory of this repository so everyone can use it to encrypt data for you.

# Sharing sensitive data

## Encrypt files

1. Ensure your cryptogaitas public key is in the `public_keys` directory.

2. Move the file you want to share to the `plain_secrets` directory

3. Run `./encrypt.sh`

The file will appear in the `encrypted_secrets` directory


## Decrypt files

1. Ensure your cryptogaitas public key is in the `public_keys` directory.

2. Move the file you want to decrypt to the `encrypted_secrets` directory

3. Run `./decrypt.sh`

The file will appear in the `plain_secrets` directory
